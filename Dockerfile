# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM external-registry.souvap-univention.de/sovereign-workplace/alpine:edge

ENV TZ Etc/UTC

LABEL maintainer="Univention <info@univention.de>"

RUN apk add --update --no-cache \
    wget \
    git \
    curl \
    bash \
    jq \
    clamav \
&& VERSION=$(curl -s "https://api.github.com/repos/google/go-containerregistry/releases/latest" | jq -r '.tag_name') \
&& OS=$(uname -o) \
&& ARCH=$(uname -m) \
&& curl -sL "https://github.com/google/go-containerregistry/releases/download/${VERSION}/go-containerregistry_${OS}_${ARCH}.tar.gz" \
    | tar xz -C /usr/local/bin/
