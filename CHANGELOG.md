# 1.0.0 (2023-12-27)


### Bug Fixes

* **ci:** Bump gitlab-config ([95765e3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/clamav-imagescan/commit/95765e3a4379c9012bd26e15e072e3cfcd028cb6))
* **ci:** Move to Open CoDE ([34fe027](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/clamav-imagescan/commit/34fe027dc21d98ba5f1a42583bffd3ac9e2d8837))


### Features

* **clamav-imagescan:** Create initial image ([aea297d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/clamav-imagescan/commit/aea297db7f92c95d8242de0457f7137074ef7f60))
